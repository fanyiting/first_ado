﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public class Darahelp
    {
        private readonly string sqlCommandString = "select * from StudentInfo";

        private readonly string connectionString = "server=.;database=ClassicDb;uid=sa;pwd=123456";

        public static void PrintUserInfo()
        {
            string connectionString = "server=.;database=ClassicDb;uid=sa;pwd=123456";

            SqlConnection sqlConnection = new SqlConnection(connectionString);

            sqlConnection.Open();

            var sqlCommandString = "select * from StudentInfo";

            SqlCommand sqlCommand = new SqlCommand(sqlCommandString, sqlConnection);

            var sdr = sqlCommand.ExecuteReader();

            Console.WriteLine("{0}\t{1}\t{2}", "代码", "姓名", "出生");

            while (sdr.Read())
            {
                Console.WriteLine("{0}\t{1}\t{2}", sdr["StudentCode"], sdr["StudentName"], sdr["Birthday"]);

            }

            sqlConnection.Close();
        }

        public void PrintUserInfoA()
        {
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();


            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommandString, connectionString);

            DataTable dataTable = new System.Data.DataTable();

            dataAdapter.Fill(dataTable);
            Console.WriteLine("{0}\t{1}\t{2}", "代码", "姓名", "出生");

            for (int i = 0; i < dataTable.Rows.Count; i++)
            {
                Console.WriteLine("{0}\t{1}\t{2}", dataTable.Rows[i]["StudentCode"], dataTable.Rows[i]["StudentName"], dataTable.Rows[i]["Birthday"]);
            }

        }
    }
}
