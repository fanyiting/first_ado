﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            //1.连接字符串
            string connStr = "server=.;database=user;uid=sa;pwd=yoshiki";

            //2.声明SqlConnection    针对数据库连接对象
            SqlConnection conn = new SqlConnection(connStr);

            //3.打开连接
            conn.Open();

            //4.指令对象
            string sql = "select * from userInfo";
            SqlCommand cmd = new SqlCommand(sql,conn);

            //5.执行SQL命令
            SqlDataReader sdr = cmd.ExecuteReader();
            while (sdr.Read())
            {
                Console.WriteLine(sdr["name"]+ "," + sdr["sex"]);
            }
            conn.Close();
        }
    }
}
