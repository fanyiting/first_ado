﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            string constr = "server=.;database=DB;uid=sa;pwd=1234567";
            SqlConnection connection = new SqlConnection(constr);
            string tsql = "select * from Users";
            SqlCommand cmd = new SqlCommand(tsql, connection);
            connection.Open();
            var read = cmd.ExecuteReader();
            Console.WriteLine("{0}\t{1}\t\t{2}", "编号","姓名","年龄");
            while (read.Read())
            {
                Console.WriteLine("{0}\t{1}\t{2}", read["UserId"], read["UserName"], read["UserAge"]);
            }
            connection.Close();
        }
    }
}
