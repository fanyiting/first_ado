﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            string connectionString = "server=.;database=Song;uid=sa;pwd=123456";

            SqlConnection sqlConnection = new SqlConnection(connectionString);

            sqlConnection.Open();

            var sqlCommandString = "select * from Table_1";

            SqlCommand sqlCommand = new SqlCommand(sqlCommandString, sqlConnection);

            var sdr = sqlCommand.ExecuteReader();

            Console.WriteLine("{0}\t{1}\t{2}\t{3}", "Id", "用户名","密码", "创建日期");

            while (sdr.Read())
            {
                Console.WriteLine("{0}\t{1}\t{2}\t{3}", sdr["Id"],sdr["Username"],sdr["password"],sdr["CreatedTime"]);
            }

            sqlConnection.Close();
        }
    }
}
