﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            string connStr = "server=.;database=test;uid=sa;pwd=123456";
            SqlConnection conn = new SqlConnection(connStr);
            conn.Open();
            string sql = "select * from userInfo";
            SqlCommand cmd = new SqlCommand(sql, conn);
            SqlDataReader sdr = cmd.ExecuteReader();
            Console.WriteLine("{0}\t{1}\t{2}", "Id", "用户名", "年龄");
            while (sdr.Read())
            {
                Console.WriteLine("{0}\t{1}\t{2}",sdr["userId"],sdr["userName"],sdr["userAge"]);
            }
            conn.Close();
        }
    }
}
