﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            ////将连接方法声明至字符串中
            //string connectionstring = "server=.;database=E_market;uid=sa;pwd=123456";
            ////创建对象
            //SqlConnection sqlconnection = new SqlConnection(connectionstring);
            ////调用方法
            //sqlconnection.Open();
            //Console.WriteLine("连接成功");
            //string constr = "server=.;uid=sa;pwd=123456;database=E_market";
            //SqlConnection sqlConnection = new SqlConnection(constr);
            //sqlConnection.Open();
            //Console.WriteLine("连接成功");
            //string constring = "Server=.;user=sa;pwd=123456;database=E_market";
            //SqlConnection mycon= new SqlConnection(constring);
            //try
            //{
            //    //mycon.Open();
            //    //string sql = "select * from Commoditysort";
            //    //SqlCommand sqlCommand = new SqlCommand(sql, mycon);
            //    //sqlCommand.CommandTimeout = 2;
            //    //Console.WriteLine("创建对象成功");
            //    mycon.Open();
            //    string sql = "insert UserInfo(userId,userPwd,UserName,Gender,Email,UserAddress,phone)values('bai','nidongde','李白',0,'lxb@198.com','福建泉州','021-3245678')";
            //    SqlCommand sqlCommand = new SqlCommand(sql, mycon);
            //    sqlCommand.ExecuteNonQuery();
            //    Console.WriteLine("数据库修改完成");

            //}
            //catch(Exception ex)
            //{
            //    Console.WriteLine(ex.Message.ToString());

            //}
            //finally {
            //    mycon.Close();

            //}
            String connectionstring = "Server=.;user=sa;pwd=123456;database=Demo";
            SqlConnection sqlConnection = new SqlConnection(connectionstring);
            try
            {
                //sqlConnection.Open();
                //Console.WriteLine("数据库连接成功");
                //string sql = "select max(age) from Employee";
                //SqlCommand sqlCommand = new SqlCommand(sql, sqlConnection);
                //sqlCommand.ExecuteScalar();
                //int t = 0;
                //t = (int)sqlCommand.ExecuteScalar();
                //Console.WriteLine("最大年龄是"+t+"岁");
                sqlConnection.Open();
                string sql = "select * from Employee ";
                SqlCommand sqlCommand = new SqlCommand(sql, sqlConnection);
                SqlDataReader zg;
                zg = sqlCommand.ExecuteReader();
                Console.WriteLine(zg.FieldCount);
                //Console.WriteLine(zg.IsClosed);
                Console.WriteLine(zg.GetDataTypeName(0));
                //  zg.Close();
                //Console.WriteLine(zg.IsClosed);
                string xty = "age";
                Console.WriteLine(zg.GetOrdinal(xty));
                if(zg.Read())
                Console.WriteLine(zg.GetValue(1));
                object[] myobj = new object[zg.FieldCount];
                zg.GetValues(myobj);
                while (zg.Read())
                {
                    zg.GetValues(myobj);
                    foreach (object outobj in myobj)Console.Write(outobj+"\t");
                    Console.WriteLine();

                }

                if (zg.HasRows)
                {
                    Console.WriteLine(" Employee中存在数据");
                }
                else
                {
                    Console.WriteLine(" Employee中不存在数据");
                }
               
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());

            }
            finally
            {
                sqlConnection.Close();

            }
        }
    }
}
