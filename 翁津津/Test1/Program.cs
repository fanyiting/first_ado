﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test1
{
    class Program
    {
        static void Main(string[] args)
        {
            //第一种
            DBhelper.PrintUserInfo();
            //第二种
            DBhelper db = new DBhelper();
            db.PrintUserInfoAD();
        }
    }
}
