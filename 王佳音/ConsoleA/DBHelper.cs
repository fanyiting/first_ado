﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleA
{
    class DBHelper
    {
        private readonly string sql= "select * from userInfo";
        private readonly string conn= "server=.;database=test6;uid=sa;pwd=123456";

        public static void PrintuserInfo()
        {
             string connStr = "server=.;database=test6;uid=sa;pwd=123456";
             SqlConnection conn = new SqlConnection(connStr);
             conn.Open();
             string sql = "select * from userInfo";
             SqlCommand cmd = new SqlCommand(sql, conn);
             SqlDataReader sdr = cmd.ExecuteReader();
             Console.WriteLine("{0}\t{1}\t{2}\t{3}\t{4}","Id","用户名","密码","年龄","性别");
             while (sdr.Read())
            {
                Console.WriteLine("{0}\t{1}\t{2}\t{3}\t{4}",sdr["userId"],sdr["userName"],sdr["userPwd"],sdr["userAge"],sdr["userSex"]);
            }
             conn.Close();
        }
        public  void PrintuserInfoAdaper()
        {
            SqlDataAdapter dataAdapter = new SqlDataAdapter(sql, conn);
            DataTable dataTable = new DataTable();
            dataAdapter.Fill(dataTable);
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("{0}\t{1}\t{2}\t{3}\t{4}", "Id", "用户名", "密码", "年龄", "性别");
            for (int i = 0; i < dataTable.Rows.Count; i++)
            {
                Console.WriteLine("{0}\t{1}\t{2}\t{3}\t{4}", dataTable.Rows[i]["userId"], dataTable.Rows[i]["userName"], dataTable.Rows[i]["userPwd"], dataTable.Rows[i]["userAge"], dataTable.Rows[i]["userSex"]);
            }
        }
    }
}
